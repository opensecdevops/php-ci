# PHP 8.1

This Dockerfile builds a PHP 8.1 image on Alpine Linux 3.17. It includes the following tools and extensions:

Composer: A tool for dependency management in PHP.
Xdebug: An extension for PHP debugging and development.
MySQL: pdo and pdo_mysql extensions for PHP.

## Additional Resources

For more details and the Docker image, visit the Harbor repository:

[PHP CI Docker Image on Harbor](https://harbor.opensecdevops.com/harbor/projects/2/repositories/php-ci/artifacts-tab/artifacts/sha256:c4d27b1286fb998148f7439533f83c2ca40f0358bbda6009a6a3f069e14086c0?publicAndNotLogged=yes)
