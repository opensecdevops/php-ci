# Docker Image Repository for PHP in CI/CD

This repository contains a collection of Dockerfile files for PHP images specifically designed for use in Continuous Integration and Continuous Deployment (CI/CD) environments. Each image is configured with essential tools like Composer, Xdebug, and MySQL to facilitate the development and testing of PHP applications.

## Available Versions

Below are links to the different versions available in this repository:

[PHP 8.1](v8.1/README.md)

## Additional Resources

For more details and download the Docker image, visit the [Harbor repository](https://harbor.opensecdevops.com/harbor/projects/2/repositories/php-ci/artifacts-tab?publicAndNotLogged=yes)
